(function(exports) {
    exports.markupToString = function(element) {
        var tmp = document.createElement('p');
        tmp.appendChild(element);
        return tmp.innerHTML;
    };
    exports.logTime = function(fn) {
        var newTime = new Date();
        console.log('time at calling',
            fn.concat(':'),
            String(newTime.getHours() + ':'),
            String(newTime.getMinutes() + ':'),
            String(newTime.getSeconds() + '.'),
            String(newTime.getMilliseconds())
        )
    };
    exports.getConfig = function(key) {
        return Config[key];
    };
    exports.setConfig = function(key, value) {
        Config[key] = value;
    };
    exports.getTrialData = function(n) {
        return Data[n];
    };
    exports.setTrialData = function(n, key, value) {
        Data[n][key] = value;
    };
    // argument a as Config.array;
    exports.sampleWithReplacement = function(a) {
        var randomIndex = Math.floor(Math.random() * a.length);
        var copyA = a.slice();
        return copyA.splice(randomIndex, 1)[0];
    };
    // argument a as Config.array (of arrays);
    exports.sampleWithoutReplacement = function(a) {
        var currentStage = Flow.getCurrentStage();
        var sampleFrom = a[currentStage];
        var randomIndex = Math.floor(Math.random() * sampleFrom.length);
        console.log(currentStage,sampleFrom,randomIndex);
        return sampleFrom.splice(randomIndex, 1)[0];
    };
    exports.decrementFill = function(i) {
        var cellsToFill = [];
        var intOfCellsBelow = parseInt(i);
        while (intOfCellsBelow > -1) {
            var cellId;
            if (intOfCellsBelow != 10) {
                cellId = String('0' + intOfCellsBelow);
            } else {
                cellId = String(intOfCellsBelow);
            }
            intOfCellsBelow--;
            cellsToFill.push(cellId);
        }
        return cellsToFill;
    };
})(this.Helpers = {});

var Trial = function() {
    this.timing = {
        'startTrial': Date.now()
    };
    this.n = Flow.currentTrial + 1;
    // identify to Flow object
    Flow.allTrials.push(this);
    Flow.currentTrial = this.n;
    if (Flow.getCurrentStage() == 99) {
        console.log(Data);
    }
    if (this.n != 0) {
        this.outcome = Helpers.sampleWithoutReplacement(Config.outcomes);
        this.coPlayerIntensity = Helpers.sampleWithoutReplacement(Config.feedbackIntensityPools);
        this.coPlayerDuration = Helpers.sampleWithoutReplacement(Config.feedbackDurationPools);
    } else {
        this.outcome = Helpers.sampleWithReplacement(Config.outcomes[0]);
        this.coPlayerIntensity = Helpers.sampleWithReplacement(Config.feedbackIntensityPools[0]);
        this.coPlayerDuration = Helpers.sampleWithReplacement(Config.feedbackDurationPools[0]);
    }
    if (this.outcome == 0) {
        this.outcomeDuration = Config.durations[this.coPlayerDuration] * 1000;
        this.revealDuration = Config.revealDuration - this.outcomeDuration;
    }
    if (!Data[this.n]) {
        Data[this.n] = {
            'coPlayerIntensity': this.coPlayerIntensity,
            'coPlayerDuration': this.coPlayerDuration,
            'outcome': this.outcome,
            'timing': this.timing
        };
    }
    return this;
};

Trial.prototype.setPlayerIntensity = function(i) {
    this.timing['setPlayerIntensity'] = Date.now();
    this.playerIntensity = i;
    Helpers.setTrialData(this.n, 'playerIntensity', this.playerIntensity);
};

Trial.prototype.setPlayerDuration = function(d) {
    this.timing['setPlayerDuration'] = Date.now();
    this.playerDuration = d;
    Helpers.setTrialData(this.n, 'playerDuration', this.playerDuration);
    if (!this.outcomeDuration) {
        this.outcomeDuration = Config.durations[this.playerDuration] * 1000;
    }
};

Trial.prototype.setPlayerReadiness = function() {
    this.timing['setPlayerReadiness'] = Date.now();
};

Trial.prototype.setTurnToRed = function() {
    this.timing['turnToRed'] = Date.now();
};

Trial.prototype.setReactToRed = function() {
    this.timing['reactToRed'] = Date.now();
    if (this.timing['reactToRed'] - this.timing['turnToRed'] > 7000) {
        this.outcome = 0;
        this.outcomeOverride = "true";
        this.outcomeDuration = Config.durations[this.playerDuration] * 1000;
        this.revealDuration = Config.revealDuration - this.outcomeDuration;
        Helpers.setTrialData(this.n, 'outcomeOverride', 'true');
    }
};

Trial.prototype.blast = function() {
    var wavLink = Config.blastURL + this.coPlayerDuration + '.wav?raw=true';
    var clip = new Audio(wavLink);
    clip.volume = this.coPlayerIntensity / 10;
    return clip;
};

(function(exports) {})(this.Data = {});

(function(exports) {
    exports.init = function() {
        var cSpan = Config.countdownSpan;
        var cDiv = Config.countdownDiv;
        var t = Date.now() + Helpers.sampleWithReplacement(Config.waitForPlayers) + Config.waitingRoomCountdownDuration;
        var timeInterval = setInterval(function(){
            var cur = Math.round((t - Date.now())/ 1000);
            if (cur < 11) {
                document.getElementById(cDiv).style.visibility = 'visible';
                document.getElementById(cSpan).innerHTML = '00:0' + String(cur);
            }
            if (cur <= -1) {
                document.getElementById(cDiv).style.visibility = 'hidden';
                //document.getElementById('continue').style.visibility = 'visible';
                //in lieu of Qualtrics buttons, automatically start trials when countdown ends
                Scales.init();
                clearInterval(timeInterval);
            }
        }, 1000);
    };
})(this.WaitingRoom = {});

(function(exports) {
    exports.init = function() {
        this.currentTrial = -1;
        this.allTrials = [];
        new Trial();
        if (!Config.debug) {
            // No instructions module; handled by Qualtrics for now
            // Instructions.init();
            WaitingRoom.init();
        } else {
            // give it a couple seconds to allow DOM to load
            window.setTimeout(function() {
                Scales.init();
            }, 500);
        }
    };
    exports.currentTrialInstance = function() {
        return this.allTrials[this.currentTrial];
    };
    exports.getCurrentStage = function() {
        var currentTrialNumber = this.currentTrial;
        if (currentTrialNumber < 1) {
            // stage 0 involves everything prior to starting trial 2
            return 0;
        } else if (currentTrialNumber > 0 && currentTrialNumber < 9) {
            return 1;
        } else if (currentTrialNumber > 8 && currentTrialNumber < 17) {
            return 2;
        } else if (currentTrialNumber > 16 && currentTrialNumber < 25) {
            return 3;
        } else {
            // stop if currentTrial is greater than 25!
            return 99;
        }
    };
    exports.trials = {
        n: 0,
        //outcome: this.randomize(),
        proceed: function() {
            this.n++;
        },
        reset: function(){},
        steps: {
        }
    };
    exports.push = function(t) {
        this.allTrials.push(t);
        if (this.allTrials.length >= 25) {
            // stop!!
            console.log('stop');
        }
    };
    exports.interTrialProceed = function() {
        if (this.getCurrentStage() != 99) {
            Target.reset(function(){
                new Trial();
            });
            Scales.reset();
        }
    };
    exports.intraTrialProceed = function() {
        if (!this.counter) {
            this.counter = [1,2,3];
        }
        this.counter.pop();
    };
})(this.Flow = {});

(function(exports) {
    exports.init = function() {
        this.render();
        this.activate();
    };
    // return array of row DOM elements
    exports.defineRows = function() {
        var abstractTable = [];
        // draw meters
        for (var i = 0; i < Config.scaleMapping.length; i++) {
            var tr = document.createElement('tr');
            var dCell = document.createElement('td');
            var iCell = document.createElement('td');
            var dText = document.createTextNode(Config.scaleMapping[i].d);
            var iText = document.createTextNode(Config.scaleMapping[i].i);
            dCell.appendChild(dText);
            iCell.appendChild(iText);
            tr.appendChild(dCell);
            tr.appendChild(iCell);
            abstractTable.push(tr);
        }
        return abstractTable;
    };
    exports.insertHeaders = function(table) {
        // TODO: find a better way to handle this!
        var headerRow = document.createElement('tr');
        var dHeaderRow = document.createElement('th');
        var iHeaderRow = document.createElement('th');
        dHeaderRow.appendChild(document.createTextNode('Duration'));
        iHeaderRow.appendChild(document.createTextNode('Intensity'));
        headerRow.appendChild(dHeaderRow);
        headerRow.appendChild(iHeaderRow);
        table.insertAdjacentHTML('afterbegin', Helpers.markupToString(headerRow));
        return table;
    };
    // return table DOM element for co-player meter
    exports.defineCoPlayerTable = function() {
        var rows = this.defineRows();
        //var table = document.createElement('table');
        var table = new Element('table', {'id': 'co-player-table'});
        for (var i = 0; i < rows.length; i++) {
            rows[i].firstChild.setAttribute('class', 'co-player-duration');
            rows[i].firstChild.setAttribute('id', ('co-player-' + Config.scaleMapping[i].did));
            rows[i].lastChild.setAttribute('class', 'co-player-intensity');
            rows[i].lastChild.setAttribute('id', ('co-player-' + Config.scaleMapping[i].iid));
            table.appendChild(rows[i]);
        }
        return table;
    };
    // return table DOM element for player meter
    exports.definePlayerTable = function() {
        var rows = this.defineRows();
        //var table = document.createElement('table');
        var table = new Element('table', {'id': 'player-table'});
        for (var i = 0; i < rows.length; i++) {
            rows[i].firstChild.setAttribute('class', 'player-duration');
            rows[i].firstChild.setAttribute('id', ('player-' + Config.scaleMapping[i].did));
            rows[i].lastChild.setAttribute('class', 'player-intensity');
            rows[i].lastChild.setAttribute('id', ('player-' + Config.scaleMapping[i].iid));
            table.appendChild(rows[i]);
        }
        return table;
    };
    exports.render = function() {
        var p = this.insertHeaders(this.definePlayerTable());
        var c = this.insertHeaders(this.defineCoPlayerTable());
        document.getElementById('player-settings').appendChild(p);
        document.getElementById('co-player-settings').appendChild(c);
    };
    exports.reset = function() {
        // clear Scale after each trial
        $('co-player-table').remove();
        $('player-table').remove();
        this.resetReadyButton();
        this.init();
        this.activate();
    };
    exports.renderCoPlayerChoices = function(callback) {
        var instance = Flow.currentTrialInstance();
        var coPlayerIntensity = instance.coPlayerIntensity;
        var coPlayerDuration = instance.coPlayerDuration;
        var intensityCellsToFillBelow = Helpers.decrementFill(coPlayerIntensity);
        var durationCellsToFillBelow = Helpers.decrementFill(coPlayerDuration);
        instance.blast().play();
        for (var i = 0; i < intensityCellsToFillBelow.length; i++) {
            var tmpID = 'co-player-intensity-' + intensityCellsToFillBelow[i];
            $(tmpID).toggleClassName('cell-selector');
        }
        for (var i = 0; i < durationCellsToFillBelow.length; i++) {
            var tmpID = 'co-player-duration-' + durationCellsToFillBelow[i];
            $(tmpID).toggleClassName('cell-selector');
        }
        window.setTimeout(function(){
            for (var i = 0; i < intensityCellsToFillBelow.length; i++) {
                var tmpID = 'co-player-intensity-' + intensityCellsToFillBelow[i];
                $(tmpID).toggleClassName('cell-selector');
            }
            for (var i = 0; i < durationCellsToFillBelow.length; i++) {
                var tmpID = 'co-player-duration-' + durationCellsToFillBelow[i];
                $(tmpID).toggleClassName('cell-selector');
            }
            callback();
        }, instance.outcomeDuration + instance.revealDuration);
    };
    exports.resetReadyButton = function() {
        Flow.counter = null;
        $('ready').toggleClassName('ready-selector');
    };
    exports.activate = function() {
        var instance = Flow.currentTrialInstance();
        var playerIntensityCells = $$('.player-intensity');
        var playerDurationCells = $$('.player-duration');
        var readyButton = $('ready');
        playerIntensityCells.each(function(el) {
            el.observe('click', function(e) {
                if (el.hasClassName('cell-selector') == false) {
                    var cellsBelow = e.target.id.slice(-2);
                    var cellsToFillBelow = Helpers.decrementFill(cellsBelow);
                    for (var i = 0; i < cellsToFillBelow.length; i++) {
                        var tmpID = 'player-intensity-' + cellsToFillBelow[i];
                        $(tmpID).toggleClassName('cell-selector');
                    }
                    instance.setPlayerIntensity(parseInt(cellsBelow));
                    Flow.intraTrialProceed();
                }
                // TODO: fix this sloppiness
                playerIntensityCells.each(function(el) {
                    el.stopObserving('click');
                });
            });
        });
        playerDurationCells.each(function(el) {
            el.observe('click', function(e) {
                if (el.hasClassName('cell-selector') == false) {
                    var cellsBelow = e.target.id.slice(-2);
                    var cellsToFillBelow = Helpers.decrementFill(cellsBelow);
                    for (var i = 0; i < cellsToFillBelow.length; i++) {
                        var tmpID = 'player-duration-' + cellsToFillBelow[i];
                        $(tmpID).toggleClassName('cell-selector');
                    }
                    instance.setPlayerDuration(parseInt(cellsBelow));
                    Flow.intraTrialProceed();
                }
                // TODO: fix this sloppiness
                playerDurationCells.each(function(el) {
                    el.stopObserving('click');
                });
            });
        });
        $('player-settings').observe('click', function(e) {
            if (Flow.counter.length == 1 && readyButton.hasClassName('ready-selector') == false) {
                readyButton.toggleClassName('ready-selector');
                instance.setPlayerReadiness();
            }
        });
        readyButton.observe('click', function(e) {
            if (readyButton.hasClassName('ready-selector')) {
                Target.init();
                readyButton.stopObserving('click');
            }
        });
    };
})(this.Scales = {});

(function(exports) {
    exports.init = function() {
        this.behave();
        this.listenForReactToRed();
    };
    exports.getTargetBehavior = function() {
        return {
            'timeToYellow': Helpers.sampleWithReplacement(Config.timeToYellow),
            'timeToRed': Helpers.sampleWithReplacement(Config.timeToRed)
        }
    };
    exports.targetToGreen = function(t) {
        (function(){
            $('target-square').style['background-color'] = 'green';
            console.log('time at turning green is: ' + Date.now());
        }).delay(t);
    };
    exports.targetToYellow = function(t) {
        //Helpers.logTime('targetToYellow');
        (function(){
            $('target-square').style['background-color'] = 'yellow';
            //Helpers.logTime('anon fn turning target to Yellow after delay');
        }).delay(t);
    };
    exports.targetToRed = function(t1, t2) {
        var target = $('target-square');
        var instance = Flow.currentTrialInstance();
        //Helpers.logTime('targetToRed');
        (function(){
            instance.setTurnToRed();
            target.style['background-color'] = 'red';
            //Helpers.logTime('anon fn turning target to Red after delay');
        }).delay(t1 + t2);
    };
    exports.behave = function() {
        //Helpers.logTime('behave');
        var behavior = this.getTargetBehavior();
        //var backToGreen = behavior / 1000;
        var timeToYellow = behavior.timeToYellow / 1000;
        var timeToRed = behavior.timeToRed / 1000;
        this.targetToYellow(timeToYellow);
        this.targetToRed(timeToRed, timeToYellow);
    };
    exports.listenForReactToRed = function() {
        var self = this;
        var instance = Flow.currentTrialInstance();
        var awaitOutcome = Helpers.sampleWithReplacement(Config.awaitOutcome);
        $('target-square').observe('click', function(e) {
            if (e.target.style['background-color'] == 'red') {
                instance.setReactToRed();
                e.target.stopObserving('click');
                self.renderOutcome(function(){
                    if (instance.outcome == 0) {
                        Scales.renderCoPlayerChoices(function(){
                            Flow.interTrialProceed();
                        });
                    } else {
                        window.setTimeout(function(){
                            Flow.interTrialProceed();
                        }, instance.outcomeDuration);
                    }
                });
            }
        });
    };
    exports.reset = function(callback) {
        var instance = Flow.currentTrialInstance();
        var outcome = instance.outcome;
        var targetSquare = $('target-square');
        window.setTimeout(function() {
            targetSquare.style.backgroundColor = 'green';
        }, Helpers.sampleWithReplacement(Config.start));
        callback();
    };
    exports.renderOutcome = function(callback) {
        var instance = Flow.currentTrialInstance();
        var outcomeDuration = instance.outcomeDuration;
        var revealDuration = instance.revealDuration;
        var outcome = instance.outcome;
        var targetSquare = $('target-square');
        var ready = $('ready');
        var target = $('target');
        targetSquare.style.backgroundColor = 'white';
        if (outcome == 0) {
            var loser = new Element('div', {'id':'loser-div'});
            loser.innerHTML = 'You lost';
            target.insertBefore(loser, targetSquare);
            window.setTimeout(function(){
                $('loser-div').remove();
            }, outcomeDuration + revealDuration);
        } else {
            var winner = new Element('div', {'id':'winner-div'});
            var winnerMessage = new Element('div', {'id':'winner-message'});
            winner.innerHTML = 'You won';
            winnerMessage.innerHTML = 'Co-Player is receiving your blast';
            target.insertBefore(winner, targetSquare);
            target.insertBefore(winnerMessage, ready);
            window.setTimeout(function(){
                $('winner-div').remove();
                $('winner-message').remove();
            }, outcomeDuration);
        }
        callback();
    };
})(this.Target = {});

(function(exports) {
    exports.demo = function(n) {
        // demo the blast during instructions
        // duration 2s, intensity 4, 7, 10
    };
    exports.renderCoPlayerBlast = function() {

    };
    exports.blastPlayer = function() {
        // if Trial instance 'outcome' is 0, blast player at 'coPlayerIntensity' & 'coPlayerDuration'
    }
})(this.Blast = {});

(function(exports) {
    exports.init = function() {
    };
    exports.proceed = function() {
        console.log('render next view');
    };
    exports.test = function(i, d) {
        var wavLink = Config.blastURL + d + '.wav?raw=true';
        var clip = new Audio(wavLink);
        clip.volume = i / 10;
        return clip;
    };
    exports.verify = function() {
        var mp3Link = Helpers.sampleWithReplacement(Config.soundcheckMapping);
        var key = Object.keys(mp3Link);
        //var val = mp3Link[key];
        return new Audio(String('public/' + key + '.mp3'));
    };
})(this.Soundcheck = {});

// configuration variables
(function(exports) {
    // WaitingRoom (array sampled with replacement)
    exports.waitForPlayers = [100, 200, 300, 500, 3200, 5300, 13300, 18300];
    // time in ms counting down to start of game
    exports.waitingRoomCountdownDuration = 10000;
    // Target (arrays sampled with replacement)
    // Target: how long is it green before changing to yellow
    exports.timeToYellow = [
        0, 
        0, 
        0, 
        100, 
        200, 
        500, 
        1000, 
        1500, 
        2000, 
        2500, 
        3000, 
        3500, 
        4000, 
        4500, 
        5000
    ];
    // Target: how long is it yellow before changing to red
    exports.timeToRed = [
        500, 
        1000, 
        1500, 
        2000, 
        2500, 
        3000
    ];
    exports.soundcheckMapping = [
        {0: 'button'},
        {1: 'computer'},
        {2: 'cupcake'},
        {3: 'energy'},
        {4: 'exaggerate'},
        {5: 'humanity'},
        {6: 'ice-cream'},
        {7: 'magazine'},
        {8: 'university'},
        {9: 'weather'}
    ];
    // Trial (arrays sampled with replacement)
    exports.awaitOutcome = [100, 200, 300, 400, 500];
    // Trial (arrays sampled without replacement; organized by stage: 0 (trial 1), 1 (trials 2-9), 2 (trials 10-17), 3 (trials 18-25)
    exports.outcomes = [
        [1],
        [0,1,0,1,0,1,0,1],
        [0,1,0,1,0,1,0,1],
        [0,1,0,1,0,1,0,1]
    ];
    exports.feedbackDurationPools = [
        [1, 2, 3, 4],
        [1, 1, 2, 2, 3, 3, 4, 4],
        [4, 4, 5, 5, 6, 6, 7, 7],
        [7, 7, 8, 8, 9, 9, 10, 10]
    ];
    exports.feedbackIntensityPools = [
        [1, 2, 3, 4],
        [1, 1, 2, 2, 3, 3, 4, 4],
        [4, 4, 5, 5, 6, 6, 7, 7],
        [7, 7, 8, 8, 9, 9, 10, 10]
    ];
    // Blast
    exports.blastURL = '//github.com/michaelnetbiz/tap-js/blob/master/blaster/blast';
    // Chat
    exports.messages = undefined;
    // Scales
    exports.countdownDiv = 'countdownDiv';
    exports.countdownSpan = 'countdownSpan';
    exports.scaleMapping = [
        {
            'd': '2s',
            'i': '10',
            'did': 'duration-10',
            'iid': 'intensity-10'
        },
        {
            'd': '1.83s',
            'i': '9',
            'did': 'duration-09',
            'iid': 'intensity-09'
        },
        {
            'd': '1.67s',
            'i': '8',
            'did': 'duration-08',
            'iid': 'intensity-08'
        },
        {
            'd': '1.5s',
            'i': '7',
            'did': 'duration-07',
            'iid': 'intensity-07'
        },
        {
            'd': '1.33s',
            'i': '6',
            'did': 'duration-06',
            'iid': 'intensity-06'
        },
        {
            'd': '1.17s',
            'i': '5',
            'did': 'duration-05',
            'iid': 'intensity-05'
        },
        {
            'd': '1s',
            'i': '4',
            'did': 'duration-04',
            'iid': 'intensity-04'
        },
        {
            'd': '0.83s',
            'i': '3',
            'did': 'duration-03',
            'iid': 'intensity-03'
        },
        {
            'd': '0.67s',
            'i': '2',
            'did': 'duration-02',
            'iid': 'intensity-02'
        },
        {
            'd': '0.5s',
            'i': '1',
            'did': 'duration-01',
            'iid': 'intensity-01'
        },
        {
            'd': '0s',
            'i': '0',
            'did': 'duration-00',
            'iid': 'intensity-00'
        }
    ];
    // Tap
    exports.version = '0.0.1';
    // Flow settings
    // maximum time in ms that player can wait in winning any trial (irrespective of any predetermined outcome)
    exports.maxWinLatency = 700;
    // time in ms co-player settings are revealed
    exports.revealDuration = 4000;
    // ms before player can select intensity/duration
    exports.durations = [0, 0.5, 0.67, 0.83, 1, 1.17, 1.33, 1.5, 1.67, 1.83, 2];
    exports.start = [500];
    // only show co-player settings on losing trials
    exports.showCoPlayerSettings = true;
    // if true, player wins first trial; if false, outcome randomly determined
    exports.trialOneOutcomePredetermined = true;
    // if true, player wins first trial; if false, player loses first trial
    exports.trialOneWin = true;
    exports.debug = true;
})(this.Config = {});

/**
 * check 1/5 of player volume
 */
//Soundcheck;

/**
 * call when player indicates readiness; should involve methods to:
 * 1) simulate co-player presence
 * 2) render ticker
 */

Flow.init();

/**
 * reacting to player selecting duration, volume by:
 * 1. rendering choice
 * 2. storing choice
 * 3. enabling 'ready' button
 */
