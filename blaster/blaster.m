% read inquisit_blast.wav, return n wav files of length 1 to n seconds
function z = blaster(wav, d)
if ~ischar(wav)
    error('Input must be of datatype char')
end
    % get variance, length (norm to 1s), rate of wav samples
    function [p,q,r] = samples_info(wav1)
        % return y (samples), Fs (sample rate) from blast.wav
        [y, Fs] = audioread(wav1);
        p = var(y);
        % q benchmark for n of samples to get 1 second long wav
        q = int64(length(y)/2);
        r = Fs;
    end

[p,q,r] = samples_info(wav);
    
    % compare variance of inquisit_blast to noise generated w/ rand()
    % function q = compare_var(q1, q2)
    %     q = var_of_samples(q1) - var(q2);
    % end

a = -1;
b = 1;

for i = 1:numel(d)
    multiplier = d(i);
    q1 = multiplier*q;
    q2 = (b-a).*rand(q1,1) + a;
    % display variances of new wav y-values
    fprintf('Variance with %s samples: %d.\n',num2str(q1),var(q2));
    % construct filename and write y-values
    this_filename = strcat('blast', num2str(i), '.wav');
    audiowrite(this_filename, q2, r); 
end

fprintf('Variance of inquisit_blast.wav: %s.\n', p);

fprintf('Created %d new blast.wav files.\n', d);
z = 'done';
end